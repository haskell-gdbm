#include <gdbm.h>
#include <stdlib.h>

GDBM_FILE
cbit_open_reader(/*const*/ char *name)
{
	return gdbm_open(name, 0, GDBM_READER, 0, NULL);
}

GDBM_FILE
cbit_open_writer(/*const*/ char *name)
{
	return gdbm_open(name, 0, GDBM_WRCREAT, 0644, NULL);
}

GDBM_FILE
cbit_open_recreate(/*const*/ char *name)
{
	return gdbm_open(name, 0, GDBM_NEWDB, 0644, NULL);
}

void
cbit_close(GDBM_FILE db)
{
	gdbm_close(db);
}

int
cbit_insert(GDBM_FILE db, void *keyp, int keysize, void *valuep, int valuesize)
{
	datum key = { keyp, keysize };
	datum value = { valuep, valuesize };
	return gdbm_store(db, key, value, GDBM_REPLACE);
}

void* cbit_fetch(GDBM_FILE db, void *keyp, int keysize, int *valuesize)
{
	datum key = { keyp, keysize };
	datum value = gdbm_fetch(db, key);
	if (value.dptr) {
		*valuesize = value.dsize;
	}
	return value.dptr;
}

int
cbit_delete(GDBM_FILE db, void *keyp, int keysize)
{
	datum key = { keyp, keysize };
	return gdbm_delete(db, key);
}

void*
cbit_firstkey(GDBM_FILE db, int *size)
{
	datum first = gdbm_firstkey(db);
	if (first.dptr) {
		*size = first.dsize;
	}
	return first.dptr;
}

void*
cbit_nextkey(GDBM_FILE db, void *keyp, int keysize, int *nextsize)
{
	datum key = { keyp, keysize };
	datum next = gdbm_nextkey(db, key);
	if (next.dptr) {
		*nextsize = next.dsize;
	}
	return next.dptr;
}

int
cbit_exists(GDBM_FILE db, void *keyp, int keysize)
{
	datum key = { keyp, keysize };
	return gdbm_exists(db, key);
}
