{-# LANGUAGE ForeignFunctionInterface,CPP #-}
module Database.GDBM.Internal where
import Foreign.Ptr
import Foreign.C

newtype GDBM = GDBM (Ptr GDBM)

foreign import ccall unsafe "cbit_open_reader"
    c_open_reader :: CString -> IO GDBM
foreign import ccall unsafe "cbit_open_writer"
    c_open_writer :: CString -> IO GDBM
foreign import ccall unsafe "cbit_open_recreate"
    c_open_recreate :: CString -> IO GDBM
foreign import ccall unsafe "cbit_close"
    c_close :: GDBM -> IO ()
foreign import ccall unsafe "cbit_insert"
    c_insert :: GDBM -> CString -> CInt -> CString -> CInt -> IO CInt
foreign import ccall unsafe "cbit_fetch"
    c_fetch :: GDBM -> CString -> CInt -> Ptr CInt -> IO CString
foreign import ccall unsafe "cbit_delete"
    c_delete :: GDBM -> CString -> CInt -> IO CInt
foreign import ccall unsafe "cbit_firstkey"
    c_firstkey :: GDBM -> Ptr CInt -> IO CString

c_null :: GDBM -> Bool
c_null (GDBM p) = p == nullPtr
